Heavy flavor documentation

This is a feature still in development and should be used only at the user´s risk.

In order to include a flavor index for a new particle with a symbolic range, the following has to be done (easier to do it with an example). The crucial point is to use as Blockname when defining the variable that serves as range of the index the name of the index itself.

IndexRange[Index[HGeneration]] = Range[5];
IndexStyle[HGeneration, Hfl];
(* ************************** *)
(* **** Particle classes **** *)
(* ************************** *)
M$ClassesDescription = {


  F[105] == {
    ClassName        -> HT,
    Indices          -> {Index[Colour],Index[HGeneration]},
    SelfConjugate    -> False,
    QuantumNumbers -> {Y -> 2/3},
    FlavorIndex      ->HGeneration,
    Mass             -> MHT,
    Width            -> 0
  }

};

M$Parameters = {

  (* External parameters *)

  (* Internal Parameters *)

  HNf == {
    ParameterType    -> Internal,
    Complexparameter -> False,
    BlockName        -> HGeneration
  },

  MHT == {
    ParameterType     -> Internal,
    Indices           -> {Index[HGeneration]},
    AllowSummation    -> True,
    ComplexParameter  -> False
  }
}
