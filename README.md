# MatchMaker

Automated tree-level and one-loop matching of general models onto general effective field theories

## Contributors

- Adrián Carmona (University of Granada)
- Achilleas Lazopoulos (ETH Zürich)
- Pablo Olgoso (University of Granada)
- Jose Santiago (University of Granada)

## Installation

Matchmakereft is available both on the PyPI Python Package Index (PyPI) https://pypi.org/project/matchmakereft/ as well as in the Anaconda Python distribution https://anaconda.org/matchmakers/matchmakereft

## Troubleshooting

We encourage users to check the troubleshooting section in the latest matchmakereft manual and the Gitlab issue tracker (https://gitlab.com/m4103/matchmaker-eft/-/issues)

## License

Matchmakereft is distributed under a Creative Commons Attribution-Noncommercial-Share Alike license

## Citation

If you use matchmakereft please cite [arXiv:2112.10787](https://arxiv.org/abs/2112.10787)

